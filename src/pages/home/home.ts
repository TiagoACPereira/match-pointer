import { Component } from '@angular/core';
import { Storage } from '@ionic/storage';
import { NavController, AlertController } from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  public data: AppComponentData = new AppComponentData;

  constructor(public navCtrl: NavController, public alertCtrl: AlertController, private storage: Storage) {
    this.loadDataFromStorage();
  }

  saveDataOnStorage() {
    this.storage.set('myMatchData', JSON.stringify(this.data));
  }

  loadDataFromStorage() {
    this.storage.get('myMatchData').then((val) => {
      if (val) {
        this.data = JSON.parse(val);
      }
    });
  }

  toggleHideEliminated(): void {
    this.data.hideEliminated = !this.data.hideEliminated;
    this.data.tooltipContentToggleEliminated = (this.data.hideEliminated) ? 'Show eliminated balls!' : 'Hide eliminated balls!';
    this.saveDataOnStorage();
  }

  onAddFoul(ball: Ball): void {
    ball.fouls++;
    this.saveDataOnStorage();

    var winnerBall = this.ballsRemaining();
    if (winnerBall.length == 1) {
      let alert = this.alertCtrl.create({
        title: 'WINNER',
        subTitle: "Ball " + winnerBall[0].number + " won with " + winnerBall[0].fouls + " fouls!!",
        buttons: [
        {
          text: 'Reset Game',
          handler: () => {
            this.onReset(true);
          }
        }
      ]});
      alert.present();
    }
  };

  onRemoveFoul(ball: Ball): void {
    if (ball.fouls > 0) {
      ball.fouls--;
      this.saveDataOnStorage();
    }
  };

  onReset(confirmed: boolean = false): void {
    var resetFun = () => {
      this.data.ballsPlaying = [
        { number: 1, fouls: 0 },
        { number: 2, fouls: 0 }
      ];
      this.saveDataOnStorage();
    };

    if (confirmed) {
      resetFun();
      return;
    }

    let confirm = this.alertCtrl.create({
      title: 'Reset Game?',
      message: 'Do you want to reset the current game?',
      buttons: [
        {
          text: 'Disagree',
          handler: () => { }
        },
        {
          text: 'Agree',
          handler: () => {
            resetFun();
          }
        }
      ]
    });

    confirm.present();
  }

  ballsRemaining(): any {
    var balls: Ball[] = [];
    this.data.ballsPlaying.forEach(ball => {
      if (!this.isEliminated(ball)) {
        balls.push(ball);
      }
    });
    return balls;
  };

  onAddBall(): void {
    var lastnumber = this.data.ballsPlaying[this.data.ballsPlaying.length - 1].number;
    if (lastnumber == 15) return;
    var newBall: Ball = { number: lastnumber + 1, fouls: 0 };
    this.data.ballsPlaying.push(newBall);
    this.saveDataOnStorage();
  };

  onRemoveLastBall(): void {
    if (this.data.ballsPlaying.length > 2) {
      this.data.ballsPlaying.pop();
      this.saveDataOnStorage();
    }
  };

  onRemoveBall(ball: Ball): void {
    if (this.data.ballsPlaying.length > 2) {
      this.data.ballsPlaying.splice(this.data.ballsPlaying.indexOf(ball), 1);
      this.saveDataOnStorage();
    }
  };

  isEliminated(ball: Ball): boolean {
    return ball.fouls == this.data.numberFoulsToEliminate;
  };

}

export class AppComponentData {
  public numberFoulsToEliminate = 3;

  public hideEliminated = false;

  public tooltipContentToggleEliminated = 'Hide eliminated balls!';

  public ballsPlaying: Ball[] = [
    { number: 1, fouls: 0 },
    { number: 2, fouls: 0 }
  ];
}


export class Ball {
  number: number = 0;
  fouls: number = 0;
}